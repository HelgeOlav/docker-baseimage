# Introduction

This Dockerfile builds my baseimage that I use for my own Docker images.

You need to login to the registry for you proceed. Add credentials to ~/.config/config.json.

Run ```docker login registry.digitalocean.com````

And run ```./build```.
