FROM alpine:latest
RUN apk update && apk add ca-certificates
COPY rootca/* /usr/local/share/ca-certificates/
RUN update-ca-certificates
